
let teams = {
        PowerRangers : [
                [44, 23, 71],
                [85, 54, 41]
        ],
        FairyTails : [
                [65, 54, 49],
                [23, 34, 47]
        ]
} 
function calcAverage (x){
        let bowl = 0
        for (let i = 0; i < x.length; i++) {
                bowl  +=  x[i]
        }
        return Math.floor(bowl / x.length);
}

let PowerRangersFirstAverage = calcAverage(teams.PowerRangers[0])
let FairyTailsFirstAverage = calcAverage(teams.FairyTails[0])
let PowerRangersSecondAverage = calcAverage(teams.PowerRangers[1])
let FairyTailsSecondAverage = calcAverage(teams.FairyTails[1])

function checkWinner(x, y){
        if(x > 2 * y){
                console.log(`PowerRangers wins  ${x} vs ${y}`)
        } else if (y > 2 * x){
                console.log(`FairyTails wins  ${y} vs ${x}`)
        } else{
                console.log('It is draw')
        }
}

checkWinner(PowerRangersFirstAverage, FairyTailsFirstAverage)
checkWinner(PowerRangersSecondAverage, FairyTailsSecondAverage)



let stats = {
        costs: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
        tips: [],
        total: [],
};
let teaMoney;
for (let i = 0; i < stats.costs.length; i++) {
         function calcTip(i) {
        if (stats.costs[i] > 50 && stats.costs[i] < 300) {
                teaMoney = stats.costs[i] * 0.15;
        } else {
             teaMoney = stats.costs[i] * 0.2;
        }
           return (teaMoney);
        }
         stats.tips.push(calcTip(i));
         stats.total.push(stats.costs[i] + stats.tips[i]);
}
       console.log(stats);
      
       let sum = 0;
       function artAve(array) {
         for (const par of array) {
          sum += par;
         }
         sum /= array.length;
         return sum;       
        }
      
console.log(artAve(stats.tips));
console.log(artAve(stats.total));      
